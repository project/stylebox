
This one is pretty simple:

stylebox.theme - A plain, table-less theme that takes advantage of a basic and print style-sheet.

print.css - Used by modern browsers when printing.
basic.css - The basic styles.
color.css - Colors are all here.

I hope that this theme might give people a basic platform for developing their themes.  Feel free to make changes.  I'd like to keep it simple and yet give people options for making changes to most parts of the theme.

This three column design is designed to degrade in a non-css browser in the following order: header, content, blocks, footer.

If you have additions or comments please let me know.

-Curtis Nelson
curtis@liminis.net
